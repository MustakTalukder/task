<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_controller extends CI_Controller {


    function __construct(){
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('twilio_model');

    }
    

 
	public function index()
	{

       $email = $this->session->userdata('email');
       $payment = $this->session->userdata('payment');

       if(empty($email)){
        redirect(base_url()."auth_controller/login");
       }elseif (empty($payment) || $payment == 0) {
           redirect(base_url()."stripe_controller/");
       }
       else{

        $this->load->view('home');
       }


    }

    public function home(){
        $this->load->view('home');
    }

    public function check_logged(){
        if($this->session->userdata('email')) {

            return true;
      }else{
          return false;
      }

    }
    


    // Start Registration Part 
    
    public function registration(){

        $this->load->view('registration');
    }

    public function is_email_available()
    {
        $email = $this->input->post('email');
        $result = $this->user_model->is_unique_email($email);
      

        //$response = $this->user_model->is_unique_email('lasjhgtone@gmail.com');

        if(empty($result)){
            $response['status'] = 'not_found';
    
            echo json_encode($response);
            
        }else{
            $response['status'] = 'already_taken';
            echo json_encode($response);

        }
  

    }
    
    
    public function register(){
        
        $this->load->library('form_validation');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $password = md5($password);

        $result = $this->user_model->is_unique_email($email);

        if(empty($result)){

            $result = $this->user_model->create_user($email, $password );

            if($result > 0){
    
                $session_data['registration_successful'] = "Account created successfully. Please login";
                $this->session->set_userdata($session_data);
    
                redirect(base_url()."auth_controller/login");
            }

        } else{

            $session_data['email_already_used'] = "Your email is already used! please try another email.";
            $this->session->set_userdata($session_data);

            redirect(base_url()."auth_controller/registration");
        }
        
        
    }
    


    public function login(){

        if($this->check_logged() == true){
            redirect(base_url()."auth_controller/home");
        }

        $this->load->view('login');
    }
    
    public function check_login(){
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $data = array(
            'email' => $email,
            'password' => $password
          //  'payment' => 1
        );

        //  $twilio_status = $this->twilio_model->get_user_twilio_info();

        $result = $this->user_model->is_user_exist($data);
        
        if($result == false){
            //$session_data['login_error'] = "Your email or password invalid";
            //$this->session->set_userdata($session_data);

            //redirect(base_url()."auth_controller/login");

            $response['status'] = 'error';
            $response['html'] = 'Your email or password invalid';
            echo json_encode($response);
           // return $response;

            


        }else{


            $_id = (string) $result->_id;
            $session_data = array();
        
    
           // var_dump($result);
    
            $session_data['_id'] = $_id;
            $session_data['email'] = $email;
            $session_data['payment'] = $result->payment;
            // $session_data['twilio_status'] = $result->twilio_status;
    
            //$session_data['login_successful'] = "Login successfully. Please pay first";
    
            $this->session->set_userdata($session_data);
            // $result['redirect'] = ture;

           // var_dump($result);
            // die();
            
    
            $response['status'] = 'success';
            $response['html'] = 'Login success';
            echo json_encode($response);
            //return $response;

            
            //redirect(base_url()."stripe_controller");
    
        }
        
    }
    



    public function logout(){
        $this->session->unset_userdata('login_error');
        $this->session->unset_userdata('_id');
        $this->session->unset_userdata('payment');
        $this->session->unset_userdata('email');
        $this->load->view('login');
    }
    



}
