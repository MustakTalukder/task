<?php
defined('BASEPATH') OR exit('No direct script access allowed');
   
class stripe_controller extends CI_Controller {
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();

       
       $this->load->model('user_model');

       $payment = $this->session->userdata('payment');

       if($payment == 1){
           redirect(base_url()."twilio_controller");
       }



       $email = $this->session->userdata('email');

       if(empty($email)){
           redirect(base_url()."auth_controller/login");
       }

       
    }
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index()
    {
        $this->load->view('my_stripe');
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function stripePost()
    {
        require_once('application/libraries/stripe-php/init.php');
    
        \Stripe\Stripe::setApiKey($this->config->item('stripe_secret'));
     
        \Stripe\Charge::create ([
                "amount" => 100 * 100,
                "currency" => "usd",
                "source" => $this->input->post('stripeToken'),
                "description" => "Test payment" 
        ]);
            
        $this->session->set_flashdata('success', 'Payment made successfully.');

        $result = $this->user_model->payment_completed();

        $session_data['payment'] = 1;
          
        $this->session->set_userdata($session_data);

        
            // redirect(base_url()."auth_controller/login");
       // redirect('/my-stripe', 'refresh');

       redirect(base_url()."twilio_controller/twilio_auth");
    }
}