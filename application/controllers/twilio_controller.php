<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Twilio\Rest\Client;


class twilio_controller extends CI_Controller {



    function __construct(){
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('twilio_model');

    }

    public function index(){

        redirect(base_url()."twilio_controller/twilio_auth");
     //   $this->load->view('twilio');

    }

    public function twilio_auth_update(){

        $this->load->view('twilio_auth_update');

    }

    public function twilio_auth_update_save(){

        $twilio_sid = $this->input->post('twilio_sid');
        $twilio_token = $this->input->post('twilio_token');

        
        $result = $this->twilio_model->save_twilio_auth_update_data($twilio_sid, $twilio_token );

        redirect(base_url()."twilio_controller/twilio_auth");

    }

    public function twilio_auth(){
        $email = $this->session->userdata('email');
        $payment = $this->session->userdata('payment');

        $twilio_status = $this->twilio_status();

       // print_r($email);
        //print_r($twilio_status);

        if(empty($email)){
            $this->load->view('login');
        }elseif (empty($payment) || $payment == 0) {
            redirect(base_url()."stripe_controller/");
        }
        elseif($twilio_status == FALSE){

            $this->load->view('twilio_auth');
        }else{


            $result = $this->twilio_model->get_user_twilio_info();

           // print_r($result->twilio_sid);

             $sid = $result->twilio_sid;
             $token = $result->twilio_token;

           //  $data['local'] = $result;
             $data['auth'] = $result;

            $this->load->view('twilio', $data);
        }
        

    }

    public function save_twilio_auth_data(){

       

        $twilio_sid = $this->input->post('twilio_sid');
        $twilio_token = $this->input->post('twilio_token');

        
        $result = $this->twilio_model->save_twilio_auth_data($twilio_sid, $twilio_token );

        $session_data['twilio_status'] = 1;

        $session_data['auth_data_added'] = "your autentication verified";
        $this->session->set_userdata($session_data);

        redirect(base_url()."twilio_controller/");


    }

    public function twilio_status(){
        $result = $this->twilio_model->get_user_twilio_info();

        if(empty($result->twilio_status)){
            print_r($result);
        }else{
            
            $result2 = $result->twilio_status;
            return $result2;
        }
        
        

        // $twilio_status = $result->twilio_status;


        // if($twilio_status == 1){
        //     $this->load->view('twilio');
        // }else{
        //     $this->load->view('twilio_auth');
        // }
    }


    public function get_data(){

        $result = $this->twilio_model->get_user_twilio_info();

       // print_r($result);

        $sid = $result->twilio_sid;
        $token = $result->twilio_token;

        //print_r($result->twilio_sid);

        $areacode = $this->input->post('areacode');

        // $sid    = "AC946f0e6e709b0501e976d5ed996dd940";
        // $token  = "cce3496147c67a25d1aea652e00e1584";



        $sid    = $sid;
        $token  = $token;

        try{
            $twilio = new Client($sid, $token);
  
  
            $data['local'] = $twilio->availablePhoneNumbers("US")
                      ->local
                      ->read(array("areaCode" => $areacode), 20);
      
                    //  print_r($data);

            $this->load->view('twilio_list', $data);
    

        } catch(Exception $error ){
            
            $session_data['twilio_auth_invalid'] = "Your auth is invalid! Please update your sid & token.";

            $this->session->set_userdata($session_data);

            redirect(base_url()."twilio_controller/twilio_auth_update");
            
        }
    }

                  
                  //  $this->load->view('twilio_list', $data);
                  //     echo "<pre>";
                  // print_r($data);
    // foreach ($local as $record) {
    //   echo $record->friendlyName;
    //     print($record->friendlyName);
    // }
                
                
    public function add_database($phonNumber){


        $result = $this->user_model->add_single_phone_number($phonNumber);

        echo "<pre>";
        print_r($result);
        

    }

    public function add_multiple_number(){

        if(isset($_POST['save']))
        {
            $checkbox = $_POST['check_list']; 
        }
        
        // echo "<pre>";
        // print_r($checkbox);

        $result = $this->user_model->get_saved_data();

        
        if(empty($result->number)){

            $result = $this->user_model->add_multiple_phone_number($checkbox);
            redirect(base_url()."twilio_controller/twilio_number_list");

       }else{

        $result = array_merge($result->number, $checkbox);

            // echo "<pre>";
            // print_r($result);

        $result = $this->user_model->add_multiple_phone_number($result);

        $session_data['add_new_numbers'] = "Add new numbers successfully";
        $this->session->set_userdata($session_data);

        redirect(base_url()."twilio_controller/twilio_number_list");

       }

    }


    public function twilio_number_list(){



        $email = $this->session->userdata('email');
        $payment = $this->session->userdata('payment');

        if(empty($email)){
            $this->load->view('login');
        }elseif (empty($payment) || $payment == 0) {
            redirect(base_url()."stripe_controller/");
        }
        else{

            $result = $this->user_model->get_saved_data();
            
            if(empty($result)){
                redirect(base_url()."twilio_controller/twilio_auth");
            }elseif (empty($result->number)) {

                $session_data['Number_not_abilable'] = "Number not found! Please buy new number";
                $this->session->set_userdata($session_data);

                redirect(base_url()."twilio_controller/twilio_auth");
            }
            else{
                
                $previous_number = $result->number;
                $data['local'] = $previous_number;
                $this->load->view('twilio_numbers', $data);
            }
        }


    }



}
