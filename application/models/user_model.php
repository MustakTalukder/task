<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class user_model extends CI_model {
	
	private $database = 'task';
	private $collection = 'users';
	private $collection_twilio = 'twilio';
	private $conn;
	
	function __construct() {
		parent::__construct();
		$this->load->library('mongodb');
		$this->conn = $this->mongodb->getConn();
    }
    

	function create_user($email, $password) {
		try {
			$user = array(
				'email' => $email,
				'password' => $password,
				'payment' => 0
			);
			
			$query = new MongoDB\Driver\BulkWrite();
			$query->insert($user);
			
			$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);

			if($result == 1) {
				return TRUE;
			}
			
			return FALSE;

		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while saving users: ' . $ex->getMessage(), 500);
		}
	}


	
	function is_user_exist($data) {

		try{

			$email = $data['email'];

			//var_dump($email);

			$filter = ['email' => $email];
			$query = new MongoDB\Driver\Query($filter);
			
			$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);

			//var_dump($result);

			$user = current($result->toArray());

			$database_password = $data['password'];
			$database_password = md5($database_password);

			if(!empty($user) && ($user->password == $database_password)){

				return $user;

			}else{
				return false;
			}

		} catch(MongoDB\Driver\Exception\RuntimeException $ex) {
			show_error('Error while fetching user: ' . $ex->getMessage(), 500);
		}

	}





	function payment_completed() {


		$email = $this->session->userdata('email');


		$query = new MongoDB\Driver\BulkWrite();
		$query->update(
			['email' => $email], 
			['$set' => array('payment' => 1)]);
		
		$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);

		return $result;

	}

	function add_single_phone_number($phone_number){

		$email = $this->session->userdata('email');

		$query = new MongoDB\Driver\BulkWrite();
		$query->update(
			['email' => $email], 
			['$set' => array('number' => $phone_number )]);
		
		$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection, $query);

	}


	function get_saved_data(){

		$email = $this->session->userdata('email');

		$filter = ['email' => $email];
		$query = new MongoDB\Driver\Query($filter);
		
		$result = $this->conn->executeQuery($this->database.'.'.$this->collection_twilio, $query);

		$result = current($result->toArray());

		return $result;
	}



	function add_multiple_phone_number($checkbox){

		$email = $this->session->userdata('email');

		$query = new MongoDB\Driver\BulkWrite();
		$query->update(
			['email' => $email], 
			//['$set' => array('number' => $phone_number )]);

			['$set' => array('number' => $checkbox )]);
		
		$result = $this->conn->executeBulkWrite($this->database.'.'.$this->collection_twilio, $query);

		return $result;

	}

	function is_unique_email($email)
	{
		$filter = ['email' => $email];
		$query = new MongoDB\Driver\Query($filter);
		
		$result = $this->conn->executeQuery($this->database.'.'.$this->collection, $query);

		$result = current($result->toArray());

		return $result;



	}



	
}