
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/parsley.css'); ?>" >

    <title>hello</title>

    <nav class="navbar navbar-dark bg-dark justify-content-center">
  <!-- Navbar content -->

    
          <ul class="nav ">
            <li class="nav-item ">
              <a class="nav-link text-white" href=" <?= base_url().'auth_controller/'  ?>">Home</a>
            </li>

            <li class="nav-item ">
              <a class="nav-link text-white" href=" <?= base_url().'twilio_controller/twilio_auth'  ?>">Search</a>
            </li>

            <li class="nav-item ">
              <a class="nav-link text-white" href=" <?= base_url().'twilio_controller/twilio_number_list'  ?>">Phone Number List</a>
            </li>

            <?php

            if(empty($this->session->userdata('email'))){
              
            
            ?>
            <li class="nav-item">
              <a class="nav-link text-white" href=" <?= base_url().'auth_controller/login'  ?> " > Login</a>
            </li>

            <?php
            }
                else{
                  
                  
                  ?>
          <li class="nav-item">
              <a class="nav-link text-white" href=" <?= base_url().'auth_controller/logout'  ?> " > Logout</a>
            </li>
            <?php
            }
            ?>

          </ul>

        </nav>
  </head>