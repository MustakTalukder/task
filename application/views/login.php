<!doctype html>
<html lang="en">

  <?php include_once('inc/header.php') ?>

  <body>
 


    <div class="row">
      <div class="col-6" style="margin: 50px auto">
        <h2>Login</h2>
        <hr> 

        <!-- <p ><?php
          $message = $this->session->userdata('login_error');
          

          if($message){
            echo "<p class='alert alert-danger'>$message</p>";
            $this->session->unset_userdata('login_error');
          }

        ?>
        </p>

        <p ><?php
          $message = $this->session->userdata('registration_successful');
          

          if($message){
            echo "<p class='alert alert-success'>$message</p>";
            $this->session->unset_userdata('registration_successful');
          }

        ?>
        </p> -->

          <div id="login_error" >
          
          </div>
          


        <!-- <?php //echo base_url().'auth_controller/check_login' ?>  -->

        <form id="form_login" data-validate="parsley" action="" method="post" >
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input  name="email" type="email" class="form-control" id="email" placeholder="Enter email" required data-parsley-type="email"  >
        
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input name="password" type="password" class="form-control" id="password" placeholder="Password" required data-parsley-length="[3,8]" >
            </div>

            <button type="submit" class="btn btn-primary">Log In</button>

            <p>Not a member? <a href="<?php echo base_url().'auth_controller/registration' ?>">Create Account</a></p>
        </form>

      </div>

    </div>

    




    <?php include_once('inc/footer.php') ?>


  <script>

  $('#form_login').parsley();

  $('#form_login').submit(function(e) {

    

   // alert("sdkf");
    
  e.preventDefault();

  var form = $(this);

  $.ajax({
      type: "POST",
      url: "<?php echo base_url().'auth_controller/check_login' ?>",
      data: form.serialize(),
      datatype: 'json',
      success: function(msg)
        {

          msg = JSON.parse(msg);
         // alert(msg.status);
         // console.log(msg);
          
          
            if(msg['status'] == 'success'){
             window.location.replace("<?php base_url('stripe_controller')?>");
            }else{
             // $('#login_error').empty();
              $('#login_error').html("<p class='alert alert-danger'>your email or password wrong new</p>");
            
            }

        }


  });

  });

</script>


  </body>



</html>